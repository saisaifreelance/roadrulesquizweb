import React, {Component, PropTypes} from 'react'
import {TextField, SelectField, PROVINCES} from './Fields'
import {Button} from 'react-bootstrap'

class Form extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {phone_number, full_name, location, onLoginButtonClick, onSubmit, onChange, buttonText, buttonStyle} = this.props
        const _phone_number_class_name = phone_number ? "waves-effect" : "waves-effect"
        return (
            <div style={{display: 'flex', flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                <div className="z-depth-1 row" style={{backgroundColor: 'white', padding: 16}}>
                    <SelectField style={{marginLeft: '3rem'}} error="" label="Province" name="location" value={location}
                                 onChange={onChange} options={PROVINCES} input_props={{}}/>
                    <TextField error="" label="Full name" name="full_name" value={full_name}
                               onChange={onChange} input_props={{}}/>

                    <div className="md-form">
                        <i className="fa fa-envelope prefix"></i>
                        <Button bsStyle={"primary"} className={"waves-effect"} onClick={onLoginButtonClick} block>
                            {buttonText}
                        </Button>
                    </div>

                    <div>
                        <Button bsStyle={"primary"} className="waves-effect pull-right" onClick={onSubmit}>
                            Login
                        </Button>
                    </div>
                </div>

            </div>
        )
    }
}


Form.propTypes = {
    phone_number: PropTypes.string.isRequired,
    full_name: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onLoginButtonClick: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
}


Form.defaultProps = {
    phone_number: '263774108008',
    full_name: 'Jabulani Mpofu',
    location: 'harare'
}

export default Form
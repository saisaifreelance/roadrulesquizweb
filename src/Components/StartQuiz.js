import React, {Component, PropTypes} from 'react'
import {QUIZ_TYPES} from '../Constants'
const { PRACTICE, TEST } = QUIZ_TYPES

class StartQuiz extends Component {
    constructor(props) {
        super(props)
    }

    onClick(quiz_type, e) {
        e.preventDefault()
        this.props.startQuiz(quiz_type)
    }

    render() {
        return (
            <div style={{display: 'flex', flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                <div className="z-depth-1 row">
                    <div onClick={this.onClick.bind(this, PRACTICE)} className="col-xs-6 waves-effect" style={{backgroundColor: 'white', padding: 16, borderBottom: '1px solid grey'}}>
                        <div style={{textAlign: 'center'}}>
                            <div style={{
                                height: 150,
                                width: 150,
                                backgroundSize: 'contain',
                                backgroundRepeat: 'no-repeat',
                                backgroundImage: `url(/img/splash_practice.png)`,
                                marginRight: 'auto',
                                marginLeft: 'auto',
                            }}/>
                            <h4>Start Practice</h4>
                        </div>
                    </div>
                    <div onClick={this.onClick.bind(this, TEST)} className="col-xs-6 waves-effect" style={{backgroundColor: 'white', padding: 16, borderLeft: '1px solid grey', borderBottom: '1px solid grey'}}>
                        <div style={{textAlign: 'center'}}>
                            <div style={{
                                height: 150,
                                width: 150,
                                backgroundSize: 'contain',
                                backgroundRepeat: 'no-repeat',
                                backgroundImage: `url(/img/splash_test.png)`,
                                marginRight: 'auto',
                                marginLeft: 'auto',
                            }}/>
                            <h4>Start Timed Test</h4>
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}


StartQuiz.propTypes = {
    startQuiz: PropTypes.func.isRequired
}

export default StartQuiz
import React, {Component, PropTypes} from 'react'
import {Answers, Question, Explanation} from './'
import icon_wrong from '../Images/close.png'
import icon_correct from '../Images/checked.png'

const options = [
    'option_1',
    'option_2',
    'option_3',
]

const Results = ({current_set}) => {
    const _results = current_set.questions.map((question, index) => {
        const response = options[Math.floor(Math.random() * 3)]
        const q = Object.assign({}, question, {response})
        return <Result question={q} key={index}/>
    })
    return (
        <div style={{backgroundColor: 'white', padding: 16}}>
            {_results}
        </div>
    )
}

const Result = ({question}) => {
    console.log(question)
    let _image = question.image ? (
        <div style={{
            height: 64,
            width: 64,
            backgroundSize: 'contain',
            backgroundRepeat: 'no-repeat',
            backgroundImage: `url(/img/${question.image}.png)`,
            marginRight: 'auto',
            marginLeft: 'auto',
        }}/>
    ) : null


    const correct = question.response === question.correct_option

    const _incorrect_option = !correct && question.response ? (
        <div style={{}}>
        <span style={{textDecoration: 'line-through', color: 'red'}}>
            <span
                style={{color: 'black'}}>{question[question.response]}</span>
        </span>
        </div>
    ) : null

    const _icon = correct ? icon_correct : icon_wrong
    const _icon_alt = correct ? "correct" : "wrong"

    return (
        <div className="media">
            <div className="media-left">
                <a href="#">
                    <img className="media-object" src={_icon} alt={_icon_alt}/>
                </a>
            </div>
            <div className="media-body">
                <h4 className="media-heading">{question.text}</h4>
                {question[question.correct_option]}
                {_incorrect_option}
            </div>
        </div>
    )

    return (
        <div>
            <h4>{question.text}</h4>
            {_image}

        </div>
    )
}

const ResultHeader = ({}) => {
    return (
        <div style={{padding: 16, backgroundColor: '#FFEB3B', minHeight: 176}}>
            <h4>
                {"header"}
            </h4>
        </div>
    )
}

class QuestionContainer extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps) {

    }

    render() {
        const {current_set, correct} = this.props
        return (
            <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
                <div className="z-depth-1">
                    <ResultHeader correct={correct} total={current_set.length}/>
                    <Results current_set={current_set}/>
                </div>

            </div>
        )
    }
}


QuestionContainer.propTypes = {}

export default QuestionContainer
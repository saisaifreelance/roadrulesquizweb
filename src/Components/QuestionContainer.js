import React, {Component, PropTypes} from 'react'
import {Answers, Question, Explanation} from './'

class QuestionContainer extends Component {
    constructor(props) {
        super(props)
        const {question} = props
        this.state = {
            show_explanation: false,
            modalScale: question.response ? 1 : 0,
            modalY: {},
            modalOpacity: 1,
        }
    }

    componentDidMount() {
        const {explanation, question} = this.props
        if (explanation && !question.response) {
            this.setState({modalScale: 1})
        }
    }

    componentDidUpdate(prevProps) {
        const {explanation, question} = this.props
        const {show_explanation} = this.state

        if (!prevProps.explanation && explanation) {
            this.setState({modalScale: 1})
        }

        if (show_explanation && prevProps.question !== question) {
            this.setState({show_explanation: false})
        }
    }

    render() {
        const {question, selected, onSelect, explanation} = this.props
        return (
            <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
                <div className="z-depth-1">
                    <Question question={question}/>
                    <Explanation explanation={explanation}
                                 question={question}
                                 selected={selected}

                    />
                    <Answers question={question} selected={selected} onSelect={onSelect}/>
                </div>

            </div>
        )
    }
}


QuestionContainer.propTypes = {}

export default QuestionContainer
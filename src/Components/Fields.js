import React, {Component, PropTypes}  from 'react'
import Select from 'react-select'
import '../select.css';
const $ = window.$

export const PROVINCES = [
    {
        value: null,
        label: "Select your province",

    },
    {
        value: "bulawayo",
        label: "Bulawayo",

    },
    {
        value: "harare",
        label: "Harare",

    },
    {
        value: "manicaland",
        label: "Manicaland",

    },
    {
        value: "mash_central",
        label: "Mash Central",

    },
    {
        value: "mash_east",
        label: "Mash East",

    },
    {
        value: "mash_west",
        label: "Mash West",

    },
    {
        value: "masvingo",
        label: "Masvingo",

    },
    {
        value: "mat_north",
        label: "Mat North",

    },
    {
        value: "mat_south",
        label: "Mat South",

    },
    {
        value: "midlands",
        label: "Midlands",

    },
]


export const TextField = (props) => {

    const {error, value, onChange, input_props, label, name} = props
    let error_msg = null

    if (error) {
        error_msg = (
            <div className="">
                <span className="">{error}</span>
            </div>
        )
    }

    return (
        <div className="md-form">
            <i className="fa fa-envelope prefix"></i>
            <input type="text"
                   id={`form-field${name}`}
                   className="form-control"
                   value={value}
                   name={name}
                   onChange={
                       (e) => {
                           e.preventDefault()
                           console.log(e)
                           onChange(name, e.target.value)

                       }
                   }
                   {...input_props}
            />
            <label>{label}</label>
        </div>
    )
}

TextField.defaultProps = {
    input_props: {}
}

export class SelectField extends Component {
    componentDidMount() {

    }

    render(props) {
        const {error, label, name, value, onChange, options, input_props} = this.props
        let error_msg = null

        if (error) {
            error_msg = (
                <div className="">
                    <span className="">{error}</span>
                </div>
            )
        }

        return (
            <div className="md-form">
                <i className="fa fa-envelope prefix"></i>
                <Select
                    name={name}
                    value={value}
                    options={options}
                    onChange={(option) => {
                        onChange(name, option.value)
                    }}
                />
            </div>
        )
    }
}

SelectField.defaultProps = {
    input_props: {}
}
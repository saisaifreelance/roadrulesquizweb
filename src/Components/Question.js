import React, {Component, PropTypes} from 'react'

class Question extends Component {

    renderImage(image) {

        if (!image)
            return null

        const uri = image ? `/img/${image}.png` : null
        return (
            <div style={{textAlign: 'center'}}>
                <div style={{
                    height: 200,
                    width: 200,
                    backgroundSize: 'contain',
                    backgroundRepeat: 'no-repeat',
                    backgroundImage: `url(${uri})`,
                    marginRight: 'auto',
                    marginLeft: 'auto',
                }}/>
            </div>
        )

    }

    render() {
        const {text, image} = this.props.question

        return (
            <div style={{padding: 16, backgroundColor: '#FFEB3B', minHeight: 176}}>
                <h4>
                    {text}
                </h4>
                {this.renderImage(image)}
            </div>
        )
    }
}


Question.propTypes = {}

export default Question
import React, {Component, PropTypes} from 'react'

const styles = {
    container: {flex: 1},
    main: {
        display: 'flex', height: '100%', flexDirection: 'column',
    }
}

class Grid extends Component {
    render() {
        return (
            <div className="row" style={styles.container}>
                <div className="col-xs-0 col-sm-2 col-md-3 col-lg-3"/>
                <div className="col-xs-12 col-sm-8 col-md-6 col-lg-6" style={styles.main}>
                    {this.props.children}
                </div>
                <div className="col-xs-0 col-sm-2 col-md-3 col-lg-3"/>
            </div>

        )
    }
}

Grid.propTypes = {}

export default Grid
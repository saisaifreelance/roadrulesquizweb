import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Navbar, NavItem, NavDropdown, MenuItem, Nav} from 'react-bootstrap'

class Header extends Component {
    render() {
        const {title} = this.props
        return (
            <Navbar className="z-depth-0" style={{marginBottom: 0, border: 'none', backgroundColor: 'transparent'}}>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="/">{title}</a>
                    </Navbar.Brand>
                </Navbar.Header>
            </Navbar>
        )
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired
}

Header.defaultProps = {
    title: "Road Rules Quiz"
}

export default Header
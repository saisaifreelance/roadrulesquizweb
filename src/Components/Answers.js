import React, {Component, PropTypes} from 'react'

const OPTION_LETTER = {
    option_1: 'A',
    option_2: 'B',
    option_3: 'C',
}

export const Answer = ({text, is_selected, onSelect, option, correct_option, response}) => {

    let backgroundColor = is_selected ? 'yellow' : 'white'

    if (response === option && response !== correct_option)
        backgroundColor = 'red'

    return (
        <div className="waves-effect"
             style={{padding: 16, backgroundColor}}
             onClick={e => {
                 e.preventDefault()
                 onSelect()
             }}
        >
            <span>{`${OPTION_LETTER[option]}. ${text}`}</span>
        </div>
    )
}

export class Answers extends Component {
    render() {
        const {question, selected, onSelect} = this.props

        const option_keys = Object.keys(OPTION_LETTER)
        const _answers = option_keys.map((option, index) => {
            console.log(option)

            return (<Answer text={question[option]}
                            onSelect={onSelect.bind(undefined, option)}
                            key={option}
                            is_selected={selected === option}
                            option={option}
                            response={question.response}
                            correct_option={question.correct_option}/>)
        })

        return (
            <div style={{padding: '16px 0 16px 0', backgroundColor: 'white'}}>
                {_answers}
            </div>
        )
    }
}

Answers.propTypes = {
    question: PropTypes.object.isRequired,
    selected: PropTypes.string,
    onSelect: PropTypes.func.isRequired,
}

export default Answers
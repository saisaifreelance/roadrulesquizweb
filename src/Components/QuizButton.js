import React, {Component, PropTypes} from 'react'
import {Button} from 'react-bootstrap'

const BUTTON_TEXT = {
    DEFAULT: 'SELECT ANSWER',
    SUBMIT: 'SUBMIT',
    CONTINUE: 'CONTINUE'
}

class QuizButton extends Component {
    render() {
        const {stage} = this.props
        let bsStyle = "default", text = BUTTON_TEXT.DEFAULT, className="waves-effect"

        switch (stage){
            case 0:{
                bsStyle = ""
                text = BUTTON_TEXT.DEFAULT
                className=""
            }
                break
            case 1:{
                bsStyle = "primary"
                text = BUTTON_TEXT.SUBMIT
            }
                break
            case 2:{
                bsStyle = "default"
                text = BUTTON_TEXT.CONTINUE
            }
                break
            default:
                bsStyle = ""

        }

        return (
            <div className="z-depth-1" style={{ backgroundColor: 'white'}}>
                <Button onClick={this.onSubmit.bind(this)} bsStyle={bsStyle} bsSize="large" style={{height: 64}} className={className}  block>{text}</Button>
            </div>
        )
    }

    onSubmit(e){
        e.preventDefault()
        this.props.onSubmit()
    }
}

QuizButton.propTypes = {
    stage: PropTypes.number.isRequired,
    onSubmit: PropTypes.func.isRequired,
}

export default QuizButton
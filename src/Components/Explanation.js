import React, {Component, PropTypes} from 'react'

class Explanation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            show_explanation: false
        }
    }

    render() {

        const {question, selected, explanation} = this.props
        if (!explanation)
            return null

        const correct = selected === question.correct_option
        const correct_answer = question[question.correct_option]
        const selected_answer = question[selected]


        const className = correct ? 'success-color' : 'danger-color'
        const titleText = correct ? "Yay, great, you go that one right" : "Oops, sorry, wrong answer."
        return (
            <div style={{padding: 16}} className={className} onClick={(e) => {
                e.preventDefault()
                this.setState({show_explanation: !this.state.show_explanation})
            }}>
                <h4>{titleText}</h4>
                <h5>{correct_answer}</h5>
                {
                    this.state.show_explanation ? (
                        <div style={{padding: 8, backgroundColor: 'white'}}>
                            <p>{question.explanation}</p>
                        </div>
                    ) : null
                }

            </div>
        )
    }
}

Explanation.propTypes = {}

export default Explanation
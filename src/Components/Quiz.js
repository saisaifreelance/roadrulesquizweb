import React, {Component, PropTypes} from 'react'
import {QuestionContainer, QuizButton} from './'

const styles = {
    container: {flex: 1},
    main: {
        display: 'flex', height: '100%', flexDirection: 'column',
    }
}

class Quiz extends Component {
    render() {
        return (
            <div className="row" style={styles.container}>
                <div className="col-xs-0 col-sm-2 col-md-3 col-lg-4"/>
                <div className="col-xs-12 col-sm-8 col-md-6 col-lg-4" style={styles.main}>
                    <QuestionContainer/>
                    <QuizButton/>
                </div>
                <div className="col-xs-0 col-sm-2 col-md-3 col-lg-4"/>
            </div>

        )
    }
}

Quiz.propTypes = {}

export default Quiz
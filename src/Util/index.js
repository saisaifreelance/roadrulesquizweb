export const createConstants = (obj) =>  {
    const ret = {}
    let key
    if (!(obj instanceof Object && !Array.isArray(obj))) {
        throw new Error('createConstants: Argument must be an object.');
    }
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            ret[key] = key
        }
    }
    return ret
}

export const getDate = (date = new Date()) => {
    let dd = date.getDate()
    let mm = date.getMonth() + 1
    let yyyy = date.getFullYear()
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return `${dd}/${mm}/${yyyy}/`
}
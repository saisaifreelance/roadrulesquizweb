import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Header from '../Components/Header'
import {Redirect, withRouter} from 'react-router-dom'
import {Button} from 'react-bootstrap'

class AppContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: null
        }
    }

    componentDidMount() {
        // this.notify('Jabulani are you here?', 'inverse')
    }

    componentWillReceiveProps(nextProps) {
        const {profile, location} = nextProps
        const {hydrated} = profile
        const {id} = profile.data

        //redirects
        let redirect = null
        if (hydrated && !id && location.pathname !== '/auth') {
            redirect = {
                pathname: '/auth',
                state: {from: location}
            }
        }
        else if (hydrated && id && location.pathname === '/auth') {
            redirect = {
                pathname: '/home',
                state: {from: location}
            }
        }
        if (redirect !== this.state.redirect) {
            this.setState({redirect})
        }
    }

    componentDidUpdate(prevProps) {
        //clear redirects
        if (this.state.redirect) {
            this.setState({redirect: null})
        }
    }

    render() {
        const {profile} = this.props
        const {redirect} = this.state
        if (!profile.hydrated)
            return null


        if (redirect)
            return (
                <div className="page-loader palette-Plus-Afrik bg">
                    <Redirect to={redirect}/>
                </div>
            )

        return (
            <div style={{height: '100%', display: 'flex',flexDirection: 'column', padding: 0}}>
                {this.props.children}
            </div>
        )
    }
}

AppContainer.propTypes = {
    profile: PropTypes.object.isRequired,
}

const mapStateToProps = (state, ownProps) => {
    const {profile} = state
    console.log(profile)
    return {
        profile
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppContainer))
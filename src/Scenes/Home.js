import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {QUIZ_TYPES} from '../Constants'
import {startQuiz, answerQuestion} from '../Actions/App'
import StartQuiz from '../Components/StartQuiz'
import Header from '../Components/Header'
import Grid from '../Components/Grid'

class Home extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {history, location, match, doStartQuiz} = this.props
        console.log(this.props)
        return (

            <div className="container" style={{height: '100%', display: 'flex', flexDirection: 'column', padding: 0}}>
                <Header/>
                <Grid>
                    <StartQuiz history={history} startQuiz={doStartQuiz}/>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const current_set = state.questions.entities.current_set
    console.log(current_set)
    return {
        current_set
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    const {history, location, match} = ownProps
    return {
        doStartQuiz(quiz_type, questions = 25){
            return dispatch(startQuiz(quiz_type, questions, history))
        },
        doAnswerQuestion(question, response){
            return dispatch(answerQuestion(question, response))
        },
    }
}

Home.propTypes = {
    current_set: PropTypes.object,
    doStartQuiz: PropTypes.func.isRequired,
    doAnswerQuestion: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
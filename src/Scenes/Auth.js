import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Form from '../Components/Form'
const document = window.document
const Digits = window.Digits
import {register, digits, renew, resend, verify} from '../Actions/Api'
import {dismissError} from '../Actions/App'
import Grid from '../Components/Grid'


const styles = {
    container: {flex: 1},
    main: {
        display: 'flex', height: '100%', flexDirection: 'column',
    }
}
class Auth extends Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                full_name: '',
                location: ''
            }
        }
    }

    componentDidMount() {
        console.log(window)
        Digits.init({consumerKey: 's9sGOAjXnQV963J49hh9DXdAR'});
    }

    onLoginButtonClick(e) {
        e.preventDefault()
        Digits.logIn()
            .done(this.onLogin.bind(this)) /*handle the response*/
            .fail(this.onLoginFailure.bind(this));
    }

    onLogin(loginResponse, other, than) {
        console.log(loginResponse)
        console.log(other)
        console.log(than)
        // Send headers to your server and validate user by calling Digits’ API
        const oAuthHeaders = loginResponse.oauth_echo_headers;
        const verifyData = {
            authHeader: oAuthHeaders['X-Verify-Credentials-Authorization'],
            apiUrl: oAuthHeaders['X-Auth-Service-Provider']
        };

        this.props.doDigits(verifyData)

    }

    onLoginFailure(err, other) {
        console.log(err)
        console.log(other)
    }

    onChange(field, value) {
        const {form: prevForm} = this.state
        const form = Object.assign({}, prevForm, {[field]: value})
        this.setState({form})
    }

    onSubmit(e) {
        e.preventDefault()

        const {digits} = this.props
        if (digits.isFetching)
            return

        if (!digits.data || !digits.data.phone_number)
            return

        let phone_number = digits.data.phone_number

        if(phone_number.indexOf("+") === 0)
            phone_number = phone_number.slice(1)


        const {form} = this.state
        const submit = Object.assign({}, form, {phone_number})
        this.props.doRegister(submit)
    }

    getButtonText(digits) {
        if (digits.isFetching)
            return digits.isFetching

        if (digits.data && digits.data.phone_number)
            return digits.data.phone_number

        return "Click to enter phone number"
    }

    render() {
        const {history, location, match, digits} = this.props
        const buttonText = this.getButtonText(digits)
        const {form} = this.state
        return (
            <Grid>
                <Form history={history} {...form}
                      onLoginButtonClick={this.onLoginButtonClick.bind(this)}
                      onSubmit={this.onSubmit.bind(this)}
                      onChange={this.onChange.bind(this)}
                      buttonText={buttonText}
                      buttonStyle={"primary"}
                />
            </Grid>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const {profile, digits} = state
    return {
        profile,
        digits,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {

        doDigits(data){
            return dispatch(digits(data))
        },

        doRegister(userDetails){
            return dispatch(register(userDetails))
        },
        doDismissError(){
            return dispatch(dismissError())
        },

    }
}

Auth.propTypes = {}

export default connect(mapStateToProps, mapDispatchToProps)(Auth)
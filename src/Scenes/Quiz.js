import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import TimerFormatter from 'minutes-seconds-milliseconds'
import {QUIZ_TYPES} from '../Constants'
import {startQuiz, answerQuestion, goResults} from '../Actions/App'
import {QuestionContainer, QuizButton} from '../Components'
import {Button} from 'react-bootstrap'
const {TEST, PRACTICE} = QUIZ_TYPES
import Header from '../Components/Header'
import Grid from '../Components/Grid'


const TEST_DURATION = 8000

class Quiz extends Component {
    constructor(props) {
        super(props)

        this.state = {
            start: null,
            time: null,
            running: true,
            selected: null,
            stage: 0,
        }

        this.renderHeader = this.renderHeader.bind(this)
    }

    componentDidMount() {
        this.setState({start: Date.now()})
        if (this.props.quiz_type === TEST)
            this.interval = setInterval(() => {
                const time = Date.now() - this.state.start
                if (time < TEST_DURATION)
                    return this.setState({
                        time: Date.now() - this.state.start
                    })

                this.setState({running: false, time: TEST_DURATION})
                clearInterval(this.interval)
            }, 100)
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }
    
    renderHeader(){
        const {current_set, quiz_type} = this.props
        const {time} = this.state
        if (this.props.quiz_type !== TEST)
            return <Header title="Practice Test"/>


        const cursor = current_set.index + 1
        const total = current_set.length
        const time_elapsed = TimerFormatter( TEST_DURATION - time)
        const total_time = TEST_DURATION

        return <Header title={time_elapsed}/>

    }

    renderMessage() {
        const {quiz_type, current_set, goResults} = this.props
        const {time} = this.state
        const data = {
            title: '',
            content: null,
            button: null,
            instruction: null
        }


        return (
            <Grid>
                <div style={{display: 'flex', flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
                    <div className="z-depth-1" style={{backgroundColor: 'white', padding: 16}}>
                        <h2 style={{textAlign: 'center'}}>Time Up!</h2>
                        <div style={{flex: 1, alignItems: 'center'}}>
                            <div style={{
                                height: 150,
                                width: 150,
                                backgroundSize: 'contain',
                                backgroundRepeat: 'no-repeat',
                                backgroundImage: `url(/img/splash_time.png)`,
                                marginRight: 'auto',
                                marginLeft: 'auto',
                            }}/>
                        </div>
                        <h4 style={{flex: 1, textAlign: 'center'}}>You have run out of time, click continue to
                            see results</h4>
                    </div>
                </div>
                <QuizButton stage={2} onSubmit={goResults}/>
            </Grid>
        )
    }

    render() {
        const {current_set, quiz_type} = this.props
        const {time, selected, running, stage} = this.state
        if (!current_set) return null
        const question = current_set.questions[current_set.index]

        let content = null


        if (!running) {
            content = this.renderMessage()
        }

        if (running && question)
            content = (
                <Grid>
                    <QuestionContainer question={question}
                                       selected={selected}
                                       onSelect={this.onSelect.bind(this)}
                                       explanation={stage === 2}
                    />
                    <QuizButton stage={stage} onSubmit={this.onSubmit.bind(this)}/>
                </Grid>
            )

        const header = this.renderHeader()

        return (
            <div className="container" style={{height: '100%', display: 'flex', flexDirection: 'column', padding: 0}}>
                {header}
                {content}
            </div>
        )
    }

    onSelect(selected) {
        this.setState({selected, stage: 1})
    }

    onSubmit() {
        const {stage, selected} = this.state
        const {quiz_type, current_set, active, doAnswerQuestion} = this.props

        switch (stage) {
            case 0: {
                return
            }
                break
            case 1: {
                return this.setState({stage: 2})
            }
                break
            case 2: {
                console.log('we are here')
                //final question
                if (!active && stage === 2 && (current_set.index + 1) >= current_set.length) {
                    this.setState({running: false})
                } else {
                    this.setState({selected: null, stage: 0})
                }
                doAnswerQuestion(current_set.questions[current_set.index], selected)
            }
                break
            default:

        }

    }
}

const mapStateToProps = (state, ownProps) => {
    const current_set = state.questions.entities.current_set
    console.log(current_set)
    return {
        current_set,
        quiz_type: TEST
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        doStartQuiz(questions = 25){
            return dispatch(startQuiz(undefined, questions))
        },
        doAnswerQuestion(question, response){
            return dispatch(answerQuestion(question, response))
        },
        goResults(){
            return dispatch(goResults(ownProps.history))
        },
    }
}

Quiz.propTypes = {
    current_set: PropTypes.object,
    doStartQuiz: PropTypes.func.isRequired,
    doAnswerQuestion: PropTypes.func.isRequired,
    quiz_type: PropTypes.oneOf([TEST, PRACTICE]).isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Quiz)
import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import Header from "../Components/Header";
import ResultsContainer from "../Components/ResultsContainer";
import QuizButton from "../Components/QuizButton";
import Grid from "../Components/Grid";

class Results extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        console.log(this.props)
        const {current_set, correct} = this.props
        return (
            <div className="container" style={{height: '100%', display: 'flex', flexDirection: 'column', padding: 0}}>
                <Header title="Results"/>
                <Grid>
                    <ResultsContainer current_set={current_set} correct={correct}/>
                    <QuizButton stage={1} onSubmit={() => {
                    }}/>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    try {
        const current_set = state.questions.entities.current_set
        const {questions} = current_set
        const correct = questions.reduce((correct, question) => {
            if (question.response === question.correct_option)
                correct++
            return correct
        }, 0)
        return {
            current_set,
            correct,
        }
    } catch (e) {
        return {}
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

Results.propTypes = {
    current_set: PropTypes.object,
    doStartQuiz: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(Results)
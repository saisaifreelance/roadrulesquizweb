import {createConstants} from './Util'

export const NAV = createConstants({
    GO_HOME: null,
    GO_PROFILE: null,
    GO_COMPANIES: null,
    GO_SERVICES: null,
    GO_PRODUCTS: null,
    GO_MESSAGES: null,
})

export const API = createConstants({
    REGISTER_MOBILE: null,
    REGISTER: null,
    VERIFY: null,
    RESEND: null,
    RENEW: null,
    DIGITS: null,
})

export const API_FLAGS = createConstants({
    SUCCESS: null,
    ERROR: null,
    PROGRESS: null,
})

export const HTTP_METHODS = createConstants({
    GET: null,
    POST: null,
})

export const GENERAL = createConstants({
    INITIALISE: null,
    START_QUIZ: null,
    ANSWER_QUESTION: null,
    REQUEST: null,
    RATE: null,
    MESSAGE: null,
    DISMISS_MESSAGE: null,
    DISMISS_ERROR: null,
    SHOW_NOTIFICATION: null,
    ADD_NOTIFICATION: null,
    OPEN_MODAL: null,
    CLOSE_MODAL: null,
    SAVE_FACEBOOK_INFO: null,
    SCAN_CONTACTS: null,
})

export const QUIZ_TYPES = createConstants({
    DIAGRAM: null,
    THEORY: null,
    TEST: null,
    PRACTICE: null,
})
import {API, API_FLAGS, GENERAL, HTTP_METHODS} from '../Constants'
const {REQUEST} = GENERAL
const {POST} = HTTP_METHODS

export const generateActivationCode = (shouldNotEqual) => {
    const min = 1000
    const max = 9999
    let activation_code = Math.floor(Math.random() * (max - min)) + min
    if (activation_code === shouldNotEqual)
        return generateActivationCode(shouldNotEqual)
    return activation_code
}

export const register = (userDetails) => (dispatch, getState) => {
    let {phone_number, location, full_name} = userDetails
    console.log(userDetails)

    phone_number = phone_number ? phone_number.trim() : phone_number
    full_name = full_name ? full_name.trim() : ''
    const parts = full_name.split(' ')
    const firstname = parts[0]
    const lastname = parts.slice(1).join(" ")

    const validation = {}

    if (!full_name)
        validation.fullname = "Full name is required"

    if (parts.length < 2)
        validation.fullname = "Full name is required"

    if (!phone_number)
        validation.phone_number = "Phone number is required"

    if (!validation.phone_number && ( phone_number.length !== 12))
        validation.phone_number = "a valid phone number in the given format is required"

    if (!location)
        validation.location = "Location is required"

    if (Object.keys(validation).length) {
        return dispatch({
            type: API.REGISTER + '_' + API_FLAGS.SUCCESS,
            payload: {
                code: 1,
                msg: "Registration Failed",
                details: validation
            }
        })
    }

    const activation_code = generateActivationCode()
    const driver_activation_code = generateActivationCode(activation_code)

    return dispatch({
        [REQUEST]: {
            type: API.REGISTER,
            endpoint: 'auth/register',
            method: POST,
            body: {
                firstname,
                lastname,
                location,
                phone_number,
                imei: 'XXXXXX',
                activation_code,
                driver_activation_code,
            }
        }
    })
}

export const digits = (body) => (dispatch, getState) => {
    return dispatch({
        [REQUEST]: {
            type: API.DIGITS,
            endpoint: 'auth/digits',
            method: POST,
            body
        }
    })
}

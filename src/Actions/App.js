import {GENERAL, QUIZ_TYPES} from '../Constants'
const {INITIALISE, START_QUIZ, ANSWER_QUESTION} = GENERAL
const { DIAGRAM, THEORY, TEST, PRACTICE } = QUIZ_TYPES

export const initialise = () => ({type: INITIALISE})

export const startQuiz = (quiz_type = TEST, questions = 25, history) => (dispatch, getState) => {
    history.push('/quiz', { quiz_type: TEST })
    return dispatch({
        type: START_QUIZ,
        quiz_type,
        questions
    })
}

export const answerQuestion = (question, response) => (dispatch, getState) => {
    const {questions} = getState()
    const action = {type: ANSWER_QUESTION, question, response}

    const {index, length} = questions.entities.current_set

    if (!((index + 1) < length ))
        action.finish = true

    dispatch(action)
}

export const goResults = (history) => (dispatch, getState) => {
    history.push('/results', { quiz_type: TEST })
}
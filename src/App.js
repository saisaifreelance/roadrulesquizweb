import React, {PropTypes} from 'react'
import {Provider} from 'react-redux'
import {BrowserRouter as Router, Route} from 'react-router-dom'

import AppContainer from './Containers/AppContainer'
import Home from './Scenes/Home'
import Quiz from './Scenes/Quiz'
import Results from './Scenes/Results'
import Auth from './Scenes/Auth'

const App = ({store, history}) => {
    return (
        <Provider store={store}>
            <Router history={history}>
                    <AppContainer>
                        <Route exact path="/" component={Home}/>
                        <Route path="/home" component={Home}/>
                        <Route path="/quiz" component={Quiz}/>
                        <Route path="/results" component={Results}/>
                        <Route path="/auth" component={Auth}/>
                    </AppContainer>
            </Router>
        </Provider>
    )
}

App.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
}
export default App

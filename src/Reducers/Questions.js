import {GENERAL} from '../Constants'
import {REHYDRATE} from 'redux-persist/constants'
import {combineReducers} from 'redux'
import {getDate} from '../Util'
const {INITIALISE, ANSWER_QUESTION, START_QUIZ} = GENERAL

export const IMAGE_RATIO = 0.6
export const QUESTION_SET_LENGTH = 25

const initialState = {
    entities: {
        diagram: {},
        theory: {},
        current_set: null,
    },
    daily_progress: {},
}

const shuffle = (array) => {
    let currentIndex = array.length, temporaryValue, randomIndex

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex]
        array[currentIndex] = array[randomIndex]
        array[randomIndex] = temporaryValue
    }

    return array
}

const loadQuestions = (questions) => {
    const entities = {
        diagram: {},
        theory: {}
    }

    for (let i = 0; i < questions.length; i++) {
        const question = questions[i]
        if (question.image)
            entities.diagram[question.id] = question
        else
            entities.theory[question.id] = question
    }
    return entities
}

const generateQuestionSet = (entities, type, questionSetLength = QUESTION_SET_LENGTH) => {
    const current_set = []

    const digs = Math.floor(questionSetLength * IMAGE_RATIO)

    let max_attempts = 0
    let i = 0
    let set = entities.diagram
    let keys = Object.keys(set)

    while (current_set.length < digs) {
        if (i >= keys.length) {
            i = 0
            max_attempts++
            continue
        }

        const question = set[keys[i]]
        const attempts = question.correctly_answered + question.incorrectly_answered

        if (question.image && max_attempts >= attempts) {
            current_set.push(Object.assign({}, question, {response: null}))
        }


        i++
    }

    max_attempts = 0
    i = 0
    set = entities.theory
    keys = Object.keys(set)

    while (current_set.length < questionSetLength) {
        if (i >= keys.length) {
            i = 0
            max_attempts++
            continue
        }

        const question = set[keys[i]]
        const attempts = question.correctly_answered + question.incorrectly_answered

        if (max_attempts >= attempts)
            current_set.push(Object.assign({}, question, {response: null}))

        i++
    }
    return shuffle(current_set)
}

export const entities = (state = initialState.entities, action) => {
    switch (action.type) {
        case INITIALISE: {
        }
            break
        case REHYDRATE:
            try {
                state = action.payload.questions.entities
                if (!Object.keys(state.diagram).length)
                    throw new Error("load questions")
            } catch (e) {
                const questions = require('../Data/questions.json')
                state = Object.assign({}, state, loadQuestions(questions))
            }
            break
        case START_QUIZ: {
            const _current_set = current_set(state.current_set, action, state)
            state = Object.assign({}, state, {current_set: _current_set})
        }
            break
        case ANSWER_QUESTION: {
            const {question} = action
            const type = question.image ? 'diagram' : 'theory'

            const _current_set = current_set(state.current_set, action, state)
            const _entity_type = entity_type(state, type, action)

            state = Object.assign({}, state, {current_set: _current_set, [type]: _entity_type})
        }
            break
        default:

    }
    return state
}

export const daily_progress = (state = initialState.daily_progress, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                state = action.payload.questions.daily_progress
            } catch (e) {
                // console.log('not found')
            }
            break
        case ANSWER_QUESTION: {
            const key = getDate()
            if (!state[key])
                state[key] = {
                    correctly_answered: 0,
                    incorrectly_answered: 0,
                }
            const option = action.question.correct_option === action.response ? 'correctly_answered' : 'incorrectly_answered'
            state[key][option] += 1
        }
            break;
        default:

    }
    return state
}

export const current_set = (state = initialState.current_set, action, entities) => {
    switch (action.type) {
        case START_QUIZ: {
            const questions = generateQuestionSet(entities, action.quiz_type, action.questions)
            const length = questions.length

            state = {
                questions,
                length,
                index: 0,
                row: 0
            }
        }
            break
        case ANSWER_QUESTION: {
            const {questions, row, length} = state
            let key = null
            for (let i = 0; i < length; i++) {
                if (questions[i].id === action.question.id) {
                    key = i
                    break
                }
            }
            if (key !== null) {
                const questionAfter = Object.assign({}, question(questions[key], action), {response: action.response})
                state = Object.assign({}, state, {
                    questions: [
                        ...questions.slice(0, key),
                        questionAfter,
                        ...questions.slice(key + 1)
                    ],
                    index: key + 1,
                    row: questionAfter.response === questionAfter.correct_option ? row + 1 : row
                })

            }
        }
            break
        default:

    }
    return state
}

export const entity_type = (state = initialState.entities, type, action) => {
    state = state[type]
    switch (action.type) {
        case ANSWER_QUESTION: {
            const {question, response} = action
            state = Object.assign({}, state, {
                [question.id]: {
                    ...state[question.id]
                }
            })
            if (question.correct_option === response) {
                state[question.id].correctly_answered += 1
            } else {
                state[question.id].incorrectly_answered += 1
            }
        }
            break
    }
    return state
}

export const question = (state = {}, action) => {
    switch (action.type) {
        case ANSWER_QUESTION: {
            let {correctly_answered, incorrectly_answered} = state
            correctly_answered += state.correct_option === action.response ? 1 : 0
            incorrectly_answered += state.correct_option === action.response ? 0 : 1
            state = Object.assign({}, state, {correctly_answered, incorrectly_answered})
        }
            break
        default:

    }
    return state
}

export default combineReducers({entities, daily_progress})

import {API, API_FLAGS, GENERAL} from '../Constants'
import {combineReducers} from 'redux'
import {REHYDRATE} from 'redux-persist/constants'

const initialState = {
    data: {},
    error: null,
    message: null,
    activation: {
        learner: {
            attempted: false,
            active: false
        },
        driver: {
            attempted: false,
            active: false
        },
    },
    isFetching: false,
    hydrated: false,
    facebook_info: {}
}

export const isFetching = (state = initialState.isFetching, action) => {
    switch (action.type) {
        case API.REGISTER:
        case API.RESEND:
        case API.RENEW:
        case API.VERIFY:
            state = true
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.RESEND + '_' + API_FLAGS.SUCCESS:
        case API.RENEW + '_' + API_FLAGS.SUCCESS:
        case API.VERIFY + '_' + API_FLAGS.SUCCESS:
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.RESEND + '_' + API_FLAGS.ERROR:
        case API.RENEW + '_' + API_FLAGS.ERROR:
        case API.VERIFY + '_' + API_FLAGS.ERROR:
            state = false
            break;
        default:

    }
    return state
}

export const data = (state = initialState.data, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                if(action.payload.profile.data)
                state = action.payload.profile.data
            } catch (e) {
                // console.log('not found')
            }
            break
        case GENERAL.INITIALISE: {
            if (action.data)
                state = action.data
            break
        }
        case API.REGISTER:
        case API.RESEND:
        case API.RENEW:
        case API.VERIFY:
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            if (code === 0) {
                state = details
            }
            else if (!code) {
                state = Object.assign({}, state, action.payload, {verified: true})
            }
        }
            break
        case API.RESEND + '_' + API_FLAGS.SUCCESS:
        case API.RENEW + '_' + API_FLAGS.SUCCESS:
        case API.VERIFY + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            if (code === 0 && details.verified) {
                state = Object.assign({}, state, details, {verified: true})
            }
        }
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.RESEND + '_' + API_FLAGS.ERROR:
        case API.RENEW + '_' + API_FLAGS.ERROR:
        case API.VERIFY + '_' + API_FLAGS.ERROR:
            break
        default:
    }
    return state
}

export const error = (state = initialState.error, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                if(action.payload.profile.error)
                state = action.payload.profile.error
            } catch (e) {
                // console.log('not found')
            }
            break
        case GENERAL.DISMISS_ERROR:
            state = null
            break
        case API.REGISTER:
        case API.RESEND:
        case API.RENEW:
        case API.VERIFY:
            state = null
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS:
        case API.RESEND + '_' + API_FLAGS.SUCCESS:
        case API.RENEW + '_' + API_FLAGS.SUCCESS:
        case API.VERIFY + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            if (code === 0) {
                state = null
            }
            else if (code === 1) {
                state = {title: msg, ...details}
            }
        }
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.RESEND + '_' + API_FLAGS.ERROR:
        case API.RENEW + '_' + API_FLAGS.ERROR:
        case API.VERIFY + '_' + API_FLAGS.ERROR:
            console.log(action)
            break
        default:
    }
    return state
}

export const message = (state = initialState.message, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                if (action.payload.profile.message)
                    state = action.payload.profile.message
            } catch (e) {
                // console.log('not found')
            }
            break
        case GENERAL.MESSAGE:
            state = action.message
            break;
        case GENERAL.ACTIVATE: {
            const section = action.activate.toLowerCase()
            state = `You have successfully activated the ${section} section!`
        }
            break;
        case GENERAL.DISMISS_MESSAGE:
            state = null
            break;
        case API.REGISTER:
        case API.RESEND:
        case API.RENEW:
        case API.VERIFY:
            break
        case API.REGISTER + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            if (code === 0) {
                state = `Welcome to the Road Rules App ${details.firstname}!`
            }
            else if (!code) {
                state = `Welcome to the Road Rules App ${action.payload.firstname}!`
            }
        }
            break;
        case API.VERIFY + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            if (code === 0) {
                state = `Welcome to the Road Rules App ${details.firstname}!`
            }
        }
            break
        case API.RESEND + '_' + API_FLAGS.SUCCESS:
        case API.RENEW + '_' + API_FLAGS.SUCCESS:
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.RESEND + '_' + API_FLAGS.ERROR:
        case API.RENEW + '_' + API_FLAGS.ERROR:
        case API.VERIFY + '_' + API_FLAGS.ERROR:
            break
        default:
    }
    return state
}

export const activation = (state = initialState.activation, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                if (action.payload.profile.activation) state = action.payload.profile.activation
            } catch (e) {
                // console.log('not found')
            }
            break
        case GENERAL.INITIALISE: {
            if (action.activation) {
                state = action.activation
            }
            break
        }

        case API.REGISTER + '_' + API_FLAGS.SUCCESS: {
            const {code, details, msg} = action.payload
            let activation = {
                learner: {
                    attempted: false,
                    active: false
                },
                driver: {
                    attempted: false,
                    active: false
                },
            }
            if (code === 0) {
                if (details.active)
                    activation.learner.active = true
                if (details.driver_active)
                    activation.driver.active = true
                state = activation
            }
        }
            break
        case API.REGISTER:
        case API.RESEND:
        case API.RENEW:
        case API.VERIFY:
            break
        case API.REGISTER + '_' + API_FLAGS.ERROR:
        case API.RESEND + '_' + API_FLAGS.ERROR:
        case API.RENEW + '_' + API_FLAGS.ERROR:
        case API.VERIFY + '_' + API_FLAGS.ERROR:
            break
        default:
    }
    return state
}

export const hydrated = (state = initialState.hydrated, action) => {
    if (action.type === REHYDRATE)
        state = true
    return state
}

export const facebook_info = (state = initialState.facebook_info, action) => {
    switch (action.type) {
        case REHYDRATE:
            try {
                if (action.payload.profile.facebook_info)
                    state = action.payload.profile.facebook_info
            } catch (e) {

            }
            break
        case GENERAL.SAVE_FACEBOOK_INFO: {
            const {data} = action
            state = data
        }
            break
        default:
    }
    return state
}

export default combineReducers({activation, data, error, isFetching, message, hydrated, facebook_info})

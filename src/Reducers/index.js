import {combineReducers} from 'redux'
import { routerReducer as router } from 'react-router-redux'
import profile from './Profile'
import questions from './Questions'
import digits from './Digits'

export default combineReducers({router, profile, questions, digits})
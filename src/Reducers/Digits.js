import {API, API_FLAGS, GENERAL} from '../Constants'
import {combineReducers} from 'redux'
import {REHYDRATE} from 'redux-persist/constants'

const initialState = {
    data: {},
    error: null,
    message: null,
    isFetching: null,
}


export default (state = initialState, action) => {
    switch (action.type) {
        case API.DIGITS: {
            state = Object.assign({}, initialState, {isFetching: 'Getting your phone number'})
        }
            break
        case API.DIGITS + '_' + API_FLAGS.SUCCESS: {
            const {payload, meta} = action
            const {code, details: data, msg} = payload
            state = Object.assign({}, initialState, {isFetching: null, data})
        }
            break
    }
    return state
}
